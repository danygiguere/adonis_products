document.addEventListener('DOMContentLoaded', function () {

    function toggleMenu() {
        if (!menuButton.classList.contains('active')) {
            menuButton.classList.add('active');
            links.classList.add('open');
            links.classList.remove('visually-hidden');
        } else {
            closeMenu();
        }
    }

    function closeMenuOnOutsideClick(event) {
        if (!links.contains(event.target) && !menuButton.contains(event.target)) {
            closeMenu();
        }
    }

    function closeMenu() {
        menuButton.classList.remove('active');
        links.classList.add('visually-hidden');
        links.addEventListener("animationend", function () {
            links.classList.remove('open');
        }, false);
    }

    // MENU
    if (document.querySelector('nav')) {

        var menuButton = document.querySelector('.menu-button');
        var links = document.querySelector('nav').querySelector('.links');

        menuButton.addEventListener("click", toggleMenu, false);
        document.addEventListener('click', closeMenuOnOutsideClick, false);
        document.addEventListener('touchstart', closeMenuOnOutsideClick, false);
    }


});
