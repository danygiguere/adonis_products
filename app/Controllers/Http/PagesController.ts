import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Product from 'App/Models/Product'

export default class PagesController {

    public async welcome ({ view }: HttpContextContract) {
        const products = await Product.query().paginate(1, 4)
        return view.render('welcome', { products })
    }

}


