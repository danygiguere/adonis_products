- Adonis V5 https://preview.adonisjs.com/blog/introducing-adonisjs-v5/ and https://preview.adonisjs.com

**********************************
- LOCAL INSTALLATION:
- copy the .env.example and add your credentials such as : 
```
PORT=3333
HOST=0.0.0.0
NODE_ENV=development
APP_KEY=6uAi51riDxmcX1eUXmxeck1b8m-1w3Rh
SESSION_DRIVER=cookie
CACHE_VIEWS=false
DB_CONNECTION=mysql
MYSQL_HOST=localhost
MYSQL_PORT=3306
MYSQL_USER=root
MYSQL_PASSWORD=root
MYSQL_DB_NAME=adonis
```

- then run :
- node ace migration:run
- node ace db:seed
**********************************

- Tutorials:
- https://www.youtube.com/watch?v=TysfaNcFX_Y&feature=emb_title
- https://www.youtube.com/watch?v=VIvBeEj9QVg&list=PL9gT3zlT0C1Ngrii-NCPpuRvUO1mIGzwf

- Commands:
- node ace --help
- node ace serve --watch
- node ace list:routes
- node ace make:controller PagesController
- node ace make:model Task

- To install the DB, do:
- npm install @adonisjs/lucid@alpha
- to set up the db:
- node ace invoke @adonisjs/lucid

- to create a migration:
- node ace make:migration tasks
- node ace migration:run
- node ace migration:rollback
- node ace migration:rollback --batch 0

- node ace db:seed

- to get protected against CSRF attacks, we must intall npm install @adonisjs/shield@alpha
- and then run node ace invoke @adonisjs/shield

- to install the auth package:
- npm install @adonisjs/auth@alpha
- npm install phc-argon2
- then: node ace invoke @adonisjs/auth
- then choose luci, web, User, true

- to run the tests, run :
- node -r @adonisjs/assembler/build/register japaFile.ts

